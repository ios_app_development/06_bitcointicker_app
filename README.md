# 06_BitcoinTicker_app

## Challenge #2.

## This is a Bitcoin price ticker.

* Give you the latest (hour) Bitcoin prices for different currencies


## Visual Presentation

### Preview
![Static image](documentation/screen_1.png)
![Gif example](https://media.giphy.com/media/7zSseeY3c0KAK7WF5J/giphy.gif)

## Possible improvements

* Add visual tracker for current bitcoin cost
* Storing of current prices for different currencies
